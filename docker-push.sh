#!/bin/sh


docker push registry.gitlab.com/alexebaker/docker_wordpress/wordpress:latest

docker push registry.gitlab.com/alexebaker/docker_wordpress/mysql:latest

docker push registry.gitlab.com/alexebaker/docker_wordpress/mariadb:latest

docker push registry.gitlab.com/alexebaker/docker_wordpress/nginx:latest
