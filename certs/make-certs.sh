#!/bin/bash

openssl req -x509 -newkey rsa:4096 -keyout nginx_key.pem -out nginx_cert.pem -days 36500 -nodes -subj '/CN=localhost'
openssl dhparam -out dhparams.pem 4096

chmod 644 nginx_cert.pem
chmod 600 nginx_key.pem
chmod 600 dhparams.pem
