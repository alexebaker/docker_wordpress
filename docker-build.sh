#!/bin/sh


docker pull wordpress:5.2-php7.3-fpm-alpine
docker pull mysql:5.7
docker pull mariadb:10.3
docker pull nginx:mainline-alpine
docker pull linuxserver/letsencrypt:latest


docker build --no-cache \
             -f dockerfiles/wordpress/Dockerfile \
             -t registry.gitlab.com/alexebaker/docker_wordpress/wordpress:latest .

docker build --no-cache \
             -f dockerfiles/mysql/Dockerfile \
             -t registry.gitlab.com/alexebaker/docker_wordpress/mysql:latest .

docker build --no-cache \
             -f dockerfiles/mariadb/Dockerfile \
             -t registry.gitlab.com/alexebaker/docker_wordpress/mariadb:latest .

docker build --no-cache \
             -f dockerfiles/nginx/Dockerfile \
             -t registry.gitlab.com/alexebaker/docker_wordpress/nginx:latest .
