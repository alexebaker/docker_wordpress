#!/bin/bash


if [[ -z "$WORDPRESS_AUTH_KEY" &&  -z "$WORDPRESS_AUTH_KEY_FILE" ]] ; then
    export WORDPRESS_AUTH_KEY="$(strings /dev/urandom | grep -o '[a-zA-Z0-9]' | head -n 64 | tr -d '\n')"
fi

if [[ -z "$WORDPRESS_SECURE_AUTH_KEY" && -z "$WORDPRESS_SECURE_AUTH_KEY_FILE" ]] ; then
    export WORDPRESS_SECURE_AUTH_KEY="$(strings /dev/urandom | grep -o '[a-zA-Z0-9]' | head -n 64 | tr -d '\n')"
fi

if [[ -z "$WORDPRESS_LOGGED_IN_KEY" && -z "$WORDPRESS_LOGGED_IN_KEY_FILE" ]] ; then
    export WORDPRESS_LOGGED_IN_KEY="$(strings /dev/urandom | grep -o '[a-zA-Z0-9]' | head -n 64 | tr -d '\n')"
fi

if [[ -z "$WORDPRESS_NONCE_KEY" && -z "$WORDPRESS_NONCE_KEY_FILE" ]] ; then
    export WORDPRESS_NONCE_KEY="$(strings /dev/urandom | grep -o '[a-zA-Z0-9]' | head -n 64 | tr -d '\n')"
fi

if [[ -z "$WORDPRESS_AUTH_SALT" && -z "$WORDPRESS_AUTH_SALT_FILE" ]] ; then
    export WORDPRESS_AUTH_SALT="$(strings /dev/urandom | grep -o '[a-zA-Z0-9]' | head -n 64 | tr -d '\n')"
fi

if [[ -z "$WORDPRESS_SECURE_AUTH_SALT" && -z "$WORDPRESS_SECURE_AUTH_SALT_FILE" ]] ; then
    export WORDPRESS_SECURE_AUTH_SALT="$(strings /dev/urandom | grep -o '[a-zA-Z0-9]' | head -n 64 | tr -d '\n')"
fi

if [[ -z "$WORDPRESS_LOGGED_IN_SALT" && -z "$WORDPRESS_LOGGED_IN_SALT_FILE" ]] ; then
    export WORDPRESS_LOGGED_IN_SALT="$(strings /dev/urandom | grep -o '[a-zA-Z0-9]' | head -n 64 | tr -d '\n')"
fi

if [[ -z "$WORDPRESS_NONCE_SALT" && -z "$WORDPRESS_NONCE_SALT_FILE" ]] ; then
    export WORDPRESS_NONCE_SALT="$(strings /dev/urandom | grep -o '[a-zA-Z0-9]' | head -n 64 | tr -d '\n')"
fi

chown -R www-data:www-data /var/www/html

cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini
sed -i 's/upload_max_filesize.*$/upload_max_filesize = 10M/g' /usr/local/etc/php/php.ini
sed -i 's/post_max_size.*$/post_max_size = 100M/g' /usr/local/etc/php/php.ini

docker-entrypoint.sh "$@"
