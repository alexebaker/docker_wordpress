#!/bin/bash


if [[ "$DEPLOY_ENV" == "Prod" ]] ; then
     sed -i '/add_header Strict-Transport-Security/s/#//g' /etc/nginx/nginx.conf
fi

if [[ -f /config/keys/letsencrypt/fullchain.pem && -f /config/keys/letsencrypt/privkey.pem ]] ; then
    cp /config/keys/letsencrypt/fullchain.pem /etc/nginx/certs/fullchain.pem
    cp /config/keys/letsencrypt/privkey.pem /etc/nginx/certs/privkey.pem
else
    cp /run/secrets/nginx_cert /etc/nginx/certs/fullchain.pem
    cp /run/secrets/nginx_key /etc/nginx/certs/privkey.pem
fi

envsubst '${URL}' < /etc/nginx/nginx.tmpl.conf > /etc/nginx/nginx.conf

if [[ -f /config/nginx/dhparams.pem && -s /config/nginx/dhparams.pem ]] ; then
    cp /config/nginx/dhparams.pem /etc/nginx/certs/dhparams.pem
else
    cp /run/secrets/nginx_dhparams /etc/nginx/certs/dhparams.pem
fi

chmod 644 /etc/nginx/certs/fullchain.pem
chmod 600 /etc/nginx/certs/privkey.pem
chmod 600 /etc/nginx/certs/dhparams.pem

crond

exec "$@"
